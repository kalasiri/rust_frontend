# Stage 1: Build the Yew application
FROM rust:latest as builder

# Install wasm-pack
RUN cargo install wasm-pack

# Create a new directory for our app
WORKDIR /app

# Copy the source code into the container
COPY . .

# Build the project using wasm-pack
RUN wasm-pack build --target web

# Stage 2: Serve the built files using nginx
FROM nginx:alpine

# Copy the built files from the builder stage
COPY --from=builder /app/pkg /usr/share/nginx/html
COPY --from=builder /app/index.html /usr/share/nginx/html
COPY --from=builder /app/styles.css /usr/share/nginx/html

# Expose port 80 to the outside world
EXPOSE 80

# Start nginx
CMD ["nginx", "-g", "daemon off;"]
