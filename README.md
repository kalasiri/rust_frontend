# Rust Web Example Frontend

This repository is dedicated to documenting my journey through *CS 410P/510 Rust Web Development* course. It serves as a personal space where I record my progress, challenges, and achievements as I delve deeper into the world of Rust for web development.

## Maintainer

- [Khalid Alasiri](mailto:kalasiri@pdx.edu)

## About the Repository

This repository contains a simple Q&A web application built using Yew for the frontend. It demonstrates core concepts of frontend development in Rust and provides a foundation for building more complex web applications with Rust.

## Functionality

The application provides a user interface to interact with the backend Q&A API. Users can view questions, add new questions, update existing questions, and delete questions.

### Features

* **View Questions**
  - Lists all questions fetched from the backend API.
* **Add Question**
  - Provides a form to add a new question.
* **Update Question**
  - Provides a form to update an existing question.
* **Delete Question**
  - Allows deleting a question by its ID.


### API Integration

The frontend application interacts with a backend API to perform CRUD operations. The base API URL is passed from the Docker environment, allowing flexibility in different deployment environments.

### Data Persistence

- The application relies on the backend API, which uses PostgreSQL for data storage. This ensures that all data remains persistent across server restarts.

## Backend 

The backend app is on this repo [Rustweb](https://gitlab.cecs.pdx.edu/kalasiri/rustweb)

## Usage

To start the application, use Docker Compose:

```bash
docker-compose up --build
```

This command builds the application from the Dockerfile and pass the base API URL from the Docker environment. The application becomes accessible on `http://localhost:8080`.


## Screenshots

![img1](screenshot/1.png)
![img2](screenshot/2.png)
![img3](screenshot/3.png)
